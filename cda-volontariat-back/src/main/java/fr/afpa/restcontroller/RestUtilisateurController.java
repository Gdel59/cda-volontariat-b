package fr.afpa.restcontroller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import fr.afpa.dao.repositories.UtilisateurRepository;

import fr.afpa.entitemetier.Personne;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;
import fr.afpa.metier.services.ServicesVoirUtilisateur;


@CrossOrigin(value ="http://localhost:3000")
@RestController

public class RestUtilisateurController { 

	@Autowired
	private IServiceUtilisateurMetier servMetier;
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	ServicesVoirUtilisateur servVoir;
	
	/**
	 * Afficher liste des utilisateurs
	 * @return
	 */
	@GetMapping("/personnes")
	public List<Personne> getPersonnes(){
		
			List<Personne> personnes =  servMetier.afficherListeUtilisateur();			
			return personnes;
	}
	
	@GetMapping("/personnes/{id}")
	public Personne getPersonne(@PathVariable("id") long id){
					
		Personne personne = servVoir.afficherUtilisateur(id);
		
			return personne;
	}
	
	@DeleteMapping("/personnes/{id}")
	public void  deletePersonne(@PathVariable("id") long id){

	if(id != 1) {
		utilisateurRepository.deleteById(id);;
	}
	
	}
	
	@PostMapping("/personnes")
	public Personne addPersonne(@RequestBody Personne personne) { 	

		return servMetier.creationPersonne(personne);	
	}
	
	@PutMapping("/updatePersonnes")
	public Personne updatePersonne(@RequestBody Personne personne) {
		return servMetier.modificationPersonne(personne);
	}
	
	@GetMapping("/volontaire")
	public List<Personne> getVolontaire(){
		return servMetier.getListVolontaire(2L);
	}
}
