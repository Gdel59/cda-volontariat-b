package fr.afpa.restcontroller;

import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.dao.repositories.AnnonceRepository;
import fr.afpa.dao.repositories.NoteRepository;
import fr.afpa.entitemetier.Annonce;
import fr.afpa.entitemetier.Note;
import fr.afpa.iservicemetier.IServiceAnnonceMetier;
import fr.afpa.iservicemetier.IServiceNoteMetier;

@CrossOrigin(value= "http://localhost:3000")
@RestController
public class RestNoteController {

	@Autowired
	private IServiceNoteMetier servNoteMetier;
	
	@Autowired
	private NoteRepository noteRepository;
	
	@PostMapping("/notes")
	public Note addNotes(@RequestBody Note note) {
		
		Long idPersonne = note.getIdPersonne();
		
		
		return servNoteMetier.ajoutNote(note, idPersonne);
	}
	
	@PutMapping("/updateNotes")
	public Note updateNote(@RequestBody Note note) {
		Long idNote = note.getIdPersonne();
		return servNoteMetier.modifierNote(note, idNote);
	}
	/**
	 * get toutes les notes de la bdd
	 * @return
	 */
	@GetMapping("/notes")
	public List<Note> getNote(){
		
		List<Note> listNote = servNoteMetier.getListNote();
		
		return listNote;
	}
	
	
	/**
	 * get les notes d'une seule personne de la bdd
	 * @param id
	 * @return
	 */
	@GetMapping("/notes/{id}")
	public List<Note> getNote(@PathVariable("id") Long id) {

		List<Note> note = servNoteMetier.getNote(id);
		return note;
	}
	
	@DeleteMapping("/deleteNote/{id}")
	public void deleteNote(@PathVariable("id") Long id) {
		noteRepository.deleteById(id);
	}
}
