package fr.afpa.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.entitemetier.Authentification;
import fr.afpa.entitemetier.Personne;
import fr.afpa.iservicemetier.IServiceAuthentificationMetier;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;
import fr.afpa.jwt.JwtUtils;

@CrossOrigin(value ="http://localhost:3000")
@RestController
public class RestAuthentificationController {

	@Autowired
	private IServiceAuthentificationMetier servauth;
	
	@Autowired
	private IServiceUtilisateurMetier servUser;
	
	
	@PostMapping("/authentification")
	public ResponseEntity<?> authentifPost(@RequestBody Authentification authentification){
					
		HttpStatus status = null;
		String token=null;
		Map<String, String> reponse = new HashMap<String, String>();
		
		Authentification auth = servauth.getAdminOrUser(authentification.getLogin(), authentification.getMotDePasse());
		List<Personne> listePersonne = servUser.afficherListeUtilisateur();
		
		if(auth != null) {
			
			for (Personne personne : listePersonne) {
				
				if(personne.getAuthentification().getIdAuthentification() == auth.getIdAuthentification() && personne.getRole().getIdRole()==1) {
					token = JwtUtils.createJWT("volontariatBDD", "front", authentification.getLogin(), "admin", 10000000000L);
					reponse.put("token", token);
					status = HttpStatus.OK;
					break;
					
				}
				else if(personne.getAuthentification().getIdAuthentification() == auth.getIdAuthentification() && personne.getRole().getIdRole()==2) {
					token = JwtUtils.createJWT("volontariatBDD", "front", authentification.getLogin(), "volontaire", 10000000000L);
					reponse.put("token", token);
					status = HttpStatus.OK;
					break;
					
				}
				else if(personne.getAuthentification().getIdAuthentification() == auth.getIdAuthentification() && personne.getRole().getIdRole()==3) {
					token = JwtUtils.createJWT("volontariatBDD", "front", authentification.getLogin(), "client", 10000000000L);
					reponse.put("token", token);
					status = HttpStatus.OK;
					break;
				}		
			}
			
		}else {
			status = HttpStatus.BAD_REQUEST;
		}
		
		return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);	
	}
	
	@GetMapping(value = "/pageprotected")
	public ResponseEntity<?> getPageProtected(@RequestHeader HttpHeaders header){
		HttpStatus status = null;
		Map<String, String> reponse = new HashMap<String, String>();
		
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if(headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if(token.isPresent() && JwtUtils.isAuthentificate(token.get(), "admin")) {
				status = HttpStatus.OK;
				reponse.put("message", "Vous etes authentifié, en tant qu'administrateur");
				return ResponseEntity.status(status).body(reponse);
			}
			if(token.isPresent() && JwtUtils.isAuthentificate(token.get(), "volontaire")) {
				status = HttpStatus.OK;
				reponse.put("message", "Vous etes authentifié en tant que volontaire");
				return ResponseEntity.status(status).body(reponse);	
			}
			if(token.isPresent() && JwtUtils.isAuthentificate(token.get(), "client")) {
				status = HttpStatus.OK;
				reponse.put("message", "Vous etes authentifié en tant que client");
				return ResponseEntity.status(status).body(reponse);
			}
		}
		
		reponse.put("message", "Vous n'etes pas authentifié");
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body(reponse);
	}
	
}
