package fr.afpa.restcontroller;

import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.dao.repositories.AnnonceRepository;
import fr.afpa.entitemetier.Annonce;
import fr.afpa.iservicemetier.IServiceAnnonceMetier;

@CrossOrigin(value= "http://localhost:3000")
@RestController
public class RestAnnonceController {

	@Autowired
	private IServiceAnnonceMetier servAnnonceMetier;
	
	@Autowired
	private AnnonceRepository annonceRepository;
	
	@PostMapping("/annonces")
	public Annonce addAnnonce(@RequestBody Annonce annonce) {
		
		Long idPersonne = annonce.getPersonne().getId();
		
		
		return servAnnonceMetier.ajoutAnnonce(annonce, idPersonne);
	}
	
	@PutMapping("/updateAnnonces")
	public Annonce updateAnnonce(@RequestBody Annonce annonce) {
		Long idAnnonce = annonce.getPersonne().getId();
		return servAnnonceMetier.modifierAnnonce(annonce, idAnnonce);
	}
	
	@GetMapping("/annonces")
	public List<Annonce> getAnnonce(){
		
		List<Annonce> listAnnonce = servAnnonceMetier.getListAnnonces();
		
		return listAnnonce;
	}
	
	
	
	@GetMapping("/annonces/{id}")
	public Annonce getAnnonce(@PathVariable("id") long id) {

		Annonce annonce = servAnnonceMetier.getAnnonce(id);
		return annonce;
	}
	
	@DeleteMapping("/deleteAnnonce/{id}")
	public void deleteAnnonce(@PathVariable("id") long id) {
		annonceRepository.deleteById(id);
	}
}
