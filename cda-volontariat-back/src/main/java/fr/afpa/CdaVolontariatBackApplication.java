package fr.afpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdaVolontariatBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdaVolontariatBackApplication.class, args);
	}

}
