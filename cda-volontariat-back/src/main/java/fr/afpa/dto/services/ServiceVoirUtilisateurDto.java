package fr.afpa.dto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.VoirUtilisateurRepository;

import fr.afpa.entitedao.PersonneDao;

import fr.afpa.entitemetier.Personne;

import fr.afpa.iservicedto.IServiceVoirUtilisateurDto;

@Service
public class ServiceVoirUtilisateurDto implements IServiceVoirUtilisateurDto {

	@Autowired
	VoirUtilisateurRepository voirPersonne;
	
	public Personne voirUser(long id) {
		PersonneDao perDao = voirPersonne.findById(id).orElse(null);
		Personne per = null;
		per = ServicesMappingDto.mappingPersonneDaoToPersonneMetier(perDao);
		      
		
		return per;
	}


	

}
