package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.AnnonceRepository;
import fr.afpa.entitedao.AnnonceDao;
import fr.afpa.entitedao.PersonneDao;
import fr.afpa.entitemetier.Annonce;
import fr.afpa.entitemetier.Personne;
import fr.afpa.iservicedto.IServiceAnnonceDto;


@Service
public class ServiceAnnonceDto implements IServiceAnnonceDto {
	
	@Autowired
	private AnnonceRepository annonceRepo;
	/**
	 * methode d'ajout d'annonce
	 */
	@Override
	public Annonce ajoutAnnonce(Annonce annonce, Personne personne) {
		
		AnnonceDao annonceDao = ServiceMappingAnnonceDto.mappingAnnonceToAnnonceDao(annonce);
		PersonneDao personneDao = ServicesMappingDto.mappingPersonneMetierToPersonneDao(personne);
		
		annonceDao.setPersonne(personneDao);
		
		
		annonceRepo.save(annonceDao);
		
		return annonce;
	}
	/**
	 * get la liste des annonces d'une personne
	 */
	@Override
	public List<Annonce> getListAnnonce(Long id) {
		List<AnnonceDao> listAnnonceDao = annonceRepo.findByPersonneIdPersonne(id);
		List<Annonce> listAnnonce = new ArrayList<Annonce>();
		if(listAnnonceDao != null) {
			for (AnnonceDao annonceDao : listAnnonceDao) {
				Annonce annonce = ServiceMappingAnnonceDto.mappingAnnonceDaoToAnnonceMetier(annonceDao);
				listAnnonce.add(annonce);
			}
		}
		return listAnnonce;
	}

	
	/**
	 * get la liste de toutes les annonces (pour admin)
	 */
	@Override
	public List<Annonce> getListAnnonces() {
		List<AnnonceDao> listAnnonceDao = annonceRepo.findAll();
		List<Annonce> listAnnonce = new ArrayList<Annonce>();
		if(listAnnonceDao != null) {
			for (AnnonceDao annonceDao : listAnnonceDao) {
				Annonce annonce = ServiceMappingAnnonceDto.mappingAnnonceDaoToAnnonceMetier(annonceDao);
				listAnnonce.add(annonce);
			}
		}
		return listAnnonce;
	}

	
	@Override
	public Annonce modificationAnnonce(Annonce annonce, Long idAnnonce) {
		AnnonceDao annonceDao = annonceRepo.findById(annonce.getId()).orElse(null);
		PersonneDao personneDao = annonceDao.getPersonne();
		annonceDao = ServiceMappingAnnonceDto.mappingAnnonceToAnnonceDao(annonce);
		annonceDao.setPersonne(personneDao);
		
		if(annonceDao != null) {
			annonceRepo.saveAndFlush(annonceDao);
			
		}
		return annonce;
	}
	@Override
	public Annonce getAnnonce(Long id) {
		// TODO Auto-generated method stub
		AnnonceDao annonceDao  = annonceRepo.findById(id).get();
		Annonce annonce = ServiceMappingAnnonceDto.mappingAnnonceDaoToAnnonceMetier(annonceDao);
		return annonce;
	}
	}


