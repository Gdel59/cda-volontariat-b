package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.NoteRepository;
import fr.afpa.entitedao.NoteDao;
import fr.afpa.entitemetier.Note;
import fr.afpa.iservicedto.IServiceNoteDto;


@Service
public class ServiceNoteDto implements IServiceNoteDto {
	
	@Autowired
	private NoteRepository noteRepo;
	
	/**
	 * methode d'ajout de note
	 */
	@Override
	public Note ajoutNote(Note note) {
		NoteDao noteDao = ServiceMappingNoteDto.mappingNoteToNoteDao(note);
		noteRepo.save(noteDao);
		
		return note;
	}

	
	/**
	 * get la liste de toutes les notes (pour admin)
	 */
	@Override
	public List<Note> getListNote() {
		List<NoteDao> listNoteDao = noteRepo.findAll();
		List<Note> listNote = new ArrayList<Note>();
		if(listNoteDao != null) {
			for (NoteDao noteDao : listNoteDao) {
				Note note = ServiceMappingNoteDto.mappingNoteDaoToNoteMetier(noteDao);
				listNote.add(note);
			}
		}
		return listNote;
	}

	/**
	 * get la liste des annonces d'une personne
	 */
	@Override
	public List<Note> getNote(Long id) {
		List<NoteDao> listNoteDao = noteRepo.findByIdPersonne(id);
		List<Note> listNote = new ArrayList<Note>();
		if(listNoteDao != null) {
			for (NoteDao noteDao : listNoteDao) {
				Note note = ServiceMappingNoteDto.mappingNoteDaoToNoteMetier(noteDao);
				listNote.add(note);
			}
		}
		return listNote;
	}
	@Override
	public Note modificationNote(Note note, Long idNote) {
		NoteDao noteDao = noteRepo.findById(idNote).orElse(null);
		
		noteDao = ServiceMappingNoteDto.mappingNoteToNoteDao(note);
		
		
		if(noteDao != null) {
			noteRepo.saveAndFlush(noteDao);
			
		}
		return note;
	}


	}


