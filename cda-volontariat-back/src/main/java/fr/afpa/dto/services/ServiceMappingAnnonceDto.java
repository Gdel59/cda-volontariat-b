package fr.afpa.dto.services;

import fr.afpa.entitedao.AnnonceDao;
import fr.afpa.entitemetier.Annonce;


public class ServiceMappingAnnonceDto {

	public static AnnonceDao mappingAnnonceToAnnonceDao(Annonce annonce) {
		AnnonceDao annonceDao = new AnnonceDao();
		annonceDao.setIdAnnonce(annonce.getId());
		annonceDao.setDate(annonce.getDate());
		annonceDao.setDescriptif(annonce.getDescriptif());
		annonceDao.setLibelle(annonce.getLibelle());
		annonceDao.setType(annonce.getType());
		
		return annonceDao;
	}

	public static Annonce mappingAnnonceDaoToAnnonceMetier(AnnonceDao annonceDao) {
		Annonce annonce = new Annonce();
		annonce.setId(annonceDao.getIdAnnonce());
		annonce.setDate(annonceDao.getDate());
		annonce.setDescriptif(annonceDao.getDescriptif());
		annonce.setPersonne(ServicesMappingDto.mappingPersonneDaoToPersonneMetier(annonceDao.getPersonne()));
		annonce.setLibelle(annonceDao.getLibelle());
		annonce.setType(annonceDao.getType());
		return annonce;
	}

}
