package fr.afpa.dto.services;

import fr.afpa.entitedao.AnnonceDao;
import fr.afpa.entitedao.NoteDao;
import fr.afpa.entitemetier.Annonce;
import fr.afpa.entitemetier.Note;

public class ServiceMappingNoteDto {

	public static NoteDao mappingNoteToNoteDao(Note note) {
		NoteDao noteDao = new NoteDao();
		noteDao.setIdNote(note.getIdNote());
		noteDao.setValeurNote(note.getValeurNote());
		noteDao.setAppreciationNote(note.getAppreciationNote());
		noteDao.setIdPersonne(note.getIdPersonne());
		
		
		return noteDao;
	}

	public static Note mappingNoteDaoToNoteMetier(NoteDao noteDao) {
		Note note = new Note();
		note.setIdNote(noteDao.getIdNote());
		note.setValeurNote(noteDao.getValeurNote());
		note.setAppreciationNote(noteDao.getAppreciationNote());
		note.setIdPersonne(noteDao.getIdPersonne());
		
		return note;
	}

}
