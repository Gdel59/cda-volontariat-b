package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.AuthentificationRepository;
import fr.afpa.dao.repositories.RoleRepository;
import fr.afpa.dao.repositories.UtilisateurRepository;
import fr.afpa.entitedao.AuthentificationDao;
import fr.afpa.entitedao.PersonneDao;
import fr.afpa.entitedao.RoleDao;
import fr.afpa.entitemetier.Admin;

import fr.afpa.entitemetier.Volontaire;
import fr.afpa.entitemetier.Personne;
import fr.afpa.entitemetier.Role;
import fr.afpa.entitemetier.Client;
import fr.afpa.iservicedto.IServiceUtilisateurDto;

@Service
public class ServiceUtilisateurDto implements IServiceUtilisateurDto {

	@Autowired
	private UtilisateurRepository personneRepository;

	@Autowired
	private AuthentificationRepository authentificationRepository;

	@Autowired
	private RoleRepository roleRepo;

	public List<Personne> listerTousLesUtilisateurs() {
		List<PersonneDao> listeUtilisateurDao = personneRepository.findAll();
		List<Personne> listeUtilisateurMetier = null;
		if (listeUtilisateurDao != null) {
			listeUtilisateurMetier = ServicesMappingDto.mappingListPersonneDaoToListPersonneMetier(listeUtilisateurDao);
			return listeUtilisateurMetier;
		}
		return listeUtilisateurMetier;
	}


	@Override
	public Role rechercheRole(Long role) {
		RoleDao r = roleRepo.findById(role).get();
		Role rm = new Role();
		rm = ServicesMappingDto.mappingRoleDaoToRoleMetier(r);
		return rm;
	}

	@Override
	public Personne creationUtilisateur(Personne personneMetier) {
		PersonneDao personneDao = ServicesMappingDto.mappingPersonneMetierToPersonneDao(personneMetier);
		RoleDao roleDao = ServicesMappingDto.mappingRoleMetierToRoleDao(personneMetier.getRole());
		AuthentificationDao authentificationDao = ServicesMappingDto.mappingAuthentificationMetierToAuthentificationDAO(personneMetier.getAuthentification());
		roleDao.getPersonne().add(personneDao);
		personneDao.setRole(roleDao);
		personneDao.setAuthentification(authentificationDao);
		authentificationDao.setPersonne(personneDao);

		 personneRepository.save(personneDao);
		 
		return personneMetier;
	
	}

	public List<Personne> getListVolontaire(Long id) {
		List<PersonneDao> listPersonneDao = personneRepository.findByRoleIdRole(id);
		List<Personne> listPersonne = new ArrayList<Personne>();
		for (PersonneDao personneDao : listPersonneDao) {
			Personne personne = ServicesMappingDto.mappingPersonneDaoToPersonneMetier(personneDao);
			listPersonne.add(personne);
		}
		return listPersonne;
	}

	public Personne getPersonne(Long id) {

		PersonneDao personneDao = personneRepository.findById(id).get();
		Personne personne = new Volontaire();
		personne = ServicesMappingDto.mappingPersonneDaoToPersonneMetier(personneDao);
		return personne;
	}

	@Override
	public Personne modificationUtilisateur(Long id) {
		PersonneDao perDao = personneRepository.findById(id).get();

		Long idrole = perDao.getRole().getIdRole();

		if (idrole == 1) {
			Personne personneMetier = new Admin();
			personneMetier = ServicesMappingDto.mappingPersonneDaoToPersonneMetier(perDao);
			personneRepository.saveAndFlush(perDao);
			return personneMetier;

		} else if (idrole == 2) {
			Personne personneMetier = new Volontaire();
			personneMetier = ServicesMappingDto.mappingPersonneDaoToPersonneMetier(perDao);
			personneRepository.saveAndFlush(perDao);
			return personneMetier;
		}

		else if (idrole == 3) {
			Personne personneMetier = new Client();
			personneMetier = ServicesMappingDto.mappingPersonneDaoToPersonneMetier(perDao);
			personneRepository.saveAndFlush(perDao);
			return personneMetier;
		}
		return null;

	}

	@Override
	public PersonneDao modifUtilisateur(Personne personneMetier) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Personne modificationUtilisateur(Personne personne) {
		
		PersonneDao perDao = personneRepository.findById(personne.getId()).get();
		AuthentificationDao authDao = perDao.getAuthentification();
		System.out.println("id authentification : "+authDao.getIdAuthentification());
		authDao.setLogin(personne.getAuthentification().getLogin());
		authDao.setPassword(personne.getAuthentification().getMotDePasse());
		perDao.setIdPersonne(personne.getId());
		perDao.setAdresse(personne.getAdresse());
		perDao.setMail(personne.getEmail());
		perDao.setNom(personne.getNom());
		perDao.setPrenom(personne.getPrenom());
		perDao.setTel(personne.getTel());
		authDao.setPersonne(perDao);
		perDao.setRole(ServicesMappingDto.mappingRoleMetierToRoleDao(personne.getRole()));

		
		authentificationRepository.saveAndFlush(authDao);
		personneRepository.saveAndFlush(perDao);
		
		
		return personne;
	}
	/*
	 * 
	 * modification utilisateur en REST à ne pas supprimer 
	@Override
	public Personne modificationUtilisateur(Personne personneMetier) {
		
		PersonneDao personneDao = ServicesMappingDto.mappingPersonneMetierToPersonneDao(personneMetier);
		RoleDao roleDao = ServicesMappingDto.mappingRoleMetierToRoleDao(personneMetier.getRole());
		AuthentificationDao authentificationDao = ServicesMappingDto.mappingAuthentificationMetierToAuthentificationDAO(personneMetier.getAuthentification());
		roleDao.getPersonne().add(personneDao);
		personneDao.setRole(roleDao);
		personneDao.setAuthentification(authentificationDao);
		authentificationDao.setPersonne(personneDao);
		
		
		authentificationRepository.saveAndFlush(authentificationDao);
		personneRepository.saveAndFlush(personneDao);
		
		
		return personneMetier;
	}*/

}
