package fr.afpa.dto.services;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import fr.afpa.dao.repositories.AuthentificationRepository;
import fr.afpa.entitedao.AuthentificationDao;
import fr.afpa.entitemetier.Authentification;
import fr.afpa.iservicedto.IServiceAuthentificationDto;

@Service
public class ServicesAuthentificationDto implements IServiceAuthentificationDto{

	@Autowired
	AuthentificationRepository authRepo;
	
	/**
	 * récupérer la personne qui se connecte
	 * selon son login et mot de passe
	 */
	public Authentification getAdminOrUser(String login, String password) {
		AuthentificationDao authDao = authRepo.findByLoginAndPassword(login, password);
		Authentification authMetier = null;
		if(authDao == null) {
			return authMetier;
		}
		else if(authRepo.existsById(authDao.getIdAuthentification())) {
			authMetier = ServicesMappingDto.mappingAuthentificationDaoToAuthentification(authDao);
			
		}
		return authMetier;
	}
	

	
	
}
