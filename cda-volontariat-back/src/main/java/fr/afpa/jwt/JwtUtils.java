package fr.afpa.jwt;



import java.util.Date;

import javax.crypto.SecretKey;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class JwtUtils {
	
	//generation d'une clé privée
    private static SecretKey SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	
    public static String createJWT(String issuer, String audience,String subject,String role, long duree) {
    	
    	return Jwts.builder()
    			  .setIssuer(issuer)
    			  .setAudience(audience)
    			  .setSubject(subject)
    			  .claim("role" , role)
    			  .setIssuedAt(new Date())
    			  .setExpiration(new Date(System.currentTimeMillis() + duree))
    			  .signWith(
    			   SECRET_KEY
    			  )
    			  .compact();
	
	}
    
    public static Jwt decodeJWT(String token)   {
    	
    	 try {
    		 return  Jwts.parserBuilder()
                .setSigningKey(SECRET_KEY)
                .build()
                .parse(token);
    	 }catch (Exception e) {
			return null;
		}

    }
    
    public static boolean isAuthentificate(String token, String role) {
    	Jwt tokenDecode = decodeJWT(token);
    	if(tokenDecode!=null) {
    			Claims claims  = (Claims) tokenDecode.getBody();
    			return role.equals(claims.get("role"));
    		
    	}
    	return false;
    }
}
