package fr.afpa.iservicedto;

import java.util.List;

import fr.afpa.entitemetier.Note;

public interface IServiceNoteDto {

	Note modificationNote(Note notes, Long idAnnonce);

	List<Note> getNote(Long id);

	List<Note> getListNote();

	Note ajoutNote(Note note);

}
