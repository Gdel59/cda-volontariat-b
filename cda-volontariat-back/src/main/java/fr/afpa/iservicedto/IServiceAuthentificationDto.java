package fr.afpa.iservicedto;

import fr.afpa.entitemetier.Authentification;

public interface IServiceAuthentificationDto {

	public Authentification getAdminOrUser(String login, String password);
}
