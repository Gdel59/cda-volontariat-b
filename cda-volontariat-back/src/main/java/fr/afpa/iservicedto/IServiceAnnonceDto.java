package fr.afpa.iservicedto;

import java.util.List;

import fr.afpa.entitemetier.Annonce;
import fr.afpa.entitemetier.Personne;

public interface IServiceAnnonceDto {

	
	public Annonce ajoutAnnonce(Annonce annonce, Personne personne);
	

	public	List<Annonce> getListAnnonces();
	public	List<Annonce> getListAnnonce(Long id);

	

	public	Annonce getAnnonce(Long id);

	public	Annonce modificationAnnonce(Annonce newAnnonce, Long idAnnonce);

	

}
