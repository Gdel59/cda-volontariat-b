package fr.afpa.iservicedto;


import fr.afpa.entitemetier.Personne;


public interface IServiceVoirUtilisateurDto {

	public Personne voirUser(long id);
	
}
