package fr.afpa.metier.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitemetier.Authentification;
import fr.afpa.iservicedto.IServiceAuthentificationDto;
import fr.afpa.iservicemetier.IServiceAuthentificationMetier;

@Service
public class ServicesAuthentificationMetier implements IServiceAuthentificationMetier {

	@Autowired
	private IServiceAuthentificationDto servDto;

	@Override
	public Authentification getAdminOrUser(String login, String password) {
		return servDto.getAdminOrUser(login, password);
	}

	
}
