package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitemetier.Note;
import fr.afpa.iservicedto.IServiceNoteDto;
import fr.afpa.iservicemetier.IServiceNoteMetier;


@Service
public class ServiceNoteMetier implements IServiceNoteMetier {

	@Autowired
	private IServiceNoteDto servNoteDto;

	@Override
	public Note ajoutNote(Note notes, Long idPersonne) {
		Note note = new Note();
		note.setIdPersonne(idPersonne);
		note.setValeurNote(notes.getValeurNote());
		note.setAppreciationNote(notes.getAppreciationNote());
		
		return servNoteDto.ajoutNote(note);
	}

	@Override
	public Note modifierNote(Note notes, Long idAnnonce) {
		
		return servNoteDto.modificationNote(notes, idAnnonce);
	}

	
	@Override
	public List<Note> getListNote() {

		return servNoteDto.getListNote();
	}

	
	/**
	 * 
	 */
	@Override
	public List<Note> getNote(Long id) {
		
		return servNoteDto.getNote(id);
	}

}
