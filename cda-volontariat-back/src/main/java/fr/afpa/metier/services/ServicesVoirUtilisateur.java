package fr.afpa.metier.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dto.services.ServiceVoirUtilisateurDto;
import fr.afpa.entitemetier.Personne;
import fr.afpa.iservicemetier.IServiceVoirUtilisateur;

@Service
public class ServicesVoirUtilisateur implements IServiceVoirUtilisateur{

	@Autowired
	private ServiceVoirUtilisateurDto servVoirDto;
	
	public Personne afficherUtilisateur(long id) {
		return servVoirDto.voirUser(id);
	}

}
