package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitemetier.Annonce;
import fr.afpa.entitemetier.Personne;
import fr.afpa.iservicedto.IServiceAnnonceDto;
import fr.afpa.iservicemetier.IServiceAnnonceMetier;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;


@Service
public class ServiceAnnonceMetier implements IServiceAnnonceMetier {

	@Autowired
	private IServiceAnnonceDto servAnnonceDto;
	
	@Autowired
	private IServiceUtilisateurMetier servUtilMetier;

	@Override
	public Annonce ajoutAnnonce(Annonce annonce, Long idPersonne) {
		
		Personne personne = servUtilMetier.getPersonne(idPersonne);
		annonce.setPersonne(personne);
				
		return servAnnonceDto.ajoutAnnonce(annonce, personne);
	}

	@Override
	public Annonce modifierAnnonce(Annonce newAnnonce, Long idAnnonce) {
		return servAnnonceDto.modificationAnnonce(newAnnonce, idAnnonce);
	}

	
	@Override
	public List<Annonce> getListAnnonces() {

		return servAnnonceDto.getListAnnonces();
	}

	

	@Override
	public List<Annonce> getListAnnonce(Long id) {
		
		return servAnnonceDto.getListAnnonce(id);
	}

	@Override
	public Annonce getAnnonce(Long id) {
		
		return servAnnonceDto.getAnnonce(id);
	}

	

}
