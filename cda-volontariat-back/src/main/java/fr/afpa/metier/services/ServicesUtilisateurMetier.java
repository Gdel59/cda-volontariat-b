package fr.afpa.metier.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entitedao.PersonneDao;


import fr.afpa.entitemetier.Personne;

import fr.afpa.iservicedto.IServiceUtilisateurDto;
import fr.afpa.iservicemetier.IServiceUtilisateurMetier;

@Service
public class ServicesUtilisateurMetier implements IServiceUtilisateurMetier {

	@Autowired
	private IServiceUtilisateurDto servDto;

	public List<Personne> afficherListeUtilisateur() {
		return servDto.listerTousLesUtilisateurs();
	}
	
	
	
	
	
	public PersonneDao modifierUtilisateur(Personne personne) {
		
		return servDto.modifUtilisateur(personne);
	}
	
	
	
	
	
/*cherche role + transmet personne*/
	@Override
	public Personne creationPersonne(Personne personne) {
		return servDto.creationUtilisateur(personne);

	}

	
	
	
	public List<Personne> getListVolontaire(Long id){
		return servDto.getListVolontaire(id);
	}
	
	
	
	public Personne getPersonne(Long id) {
		return servDto.getPersonne(id);
	}


	
	
	public Personne modificationPersonne(Personne personne) {
		return servDto.modificationUtilisateur(personne);

	}


}
