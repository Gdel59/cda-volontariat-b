package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;


import fr.afpa.entitedao.AuthentificationDao;


public interface AuthentificationRepository extends JpaRepository <AuthentificationDao, Long> {
	AuthentificationDao findByLoginAndPassword(String login, String password);
}
