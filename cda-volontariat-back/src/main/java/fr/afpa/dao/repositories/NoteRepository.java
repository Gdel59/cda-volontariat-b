package fr.afpa.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.NoteDao;
import fr.afpa.entitemetier.Annonce;
import fr.afpa.entitemetier.Note;

public interface NoteRepository extends JpaRepository<NoteDao, Long>{

	List<Note> findByPersonneIdPersonne(Long id);
	
	List<NoteDao> findByIdPersonne(Long idPersonne);
}
