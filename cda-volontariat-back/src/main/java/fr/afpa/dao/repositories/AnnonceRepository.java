package fr.afpa.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.entitedao.AnnonceDao;

public interface AnnonceRepository extends JpaRepository<AnnonceDao, Long>{

	List<AnnonceDao> findByPersonneIdPersonne(Long id);
	
	

}
