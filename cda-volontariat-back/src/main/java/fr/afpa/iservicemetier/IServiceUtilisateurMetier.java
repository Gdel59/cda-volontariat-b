package fr.afpa.iservicemetier;


import java.util.List;

import fr.afpa.entitedao.PersonneDao;

import fr.afpa.entitemetier.Personne;


public interface IServiceUtilisateurMetier {

	public List<Personne> afficherListeUtilisateur(); 
	
	public PersonneDao modifierUtilisateur(Personne personne);
	
	public Personne creationPersonne(Personne personne);

	public List<Personne> getListVolontaire(Long id);
	
	public Personne getPersonne(Long id);

	public Personne modificationPersonne(Personne personne);

 
}
