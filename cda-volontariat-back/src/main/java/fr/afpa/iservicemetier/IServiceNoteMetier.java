package fr.afpa.iservicemetier;

import java.util.List;

import fr.afpa.entitemetier.Note;

public interface IServiceNoteMetier {

	Note ajoutNote(Note note, Long idPersonne);

	Note modifierNote(Note note, Long idNote);

	List<Note> getListNote();

	

	List<Note> getNote(Long id);

}
