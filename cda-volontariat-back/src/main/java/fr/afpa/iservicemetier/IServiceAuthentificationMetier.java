package fr.afpa.iservicemetier;

import fr.afpa.entitemetier.Authentification;

public interface IServiceAuthentificationMetier {
	public Authentification getAdminOrUser(String login, String password);
}
