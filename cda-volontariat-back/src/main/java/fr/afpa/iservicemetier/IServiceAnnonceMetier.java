package fr.afpa.iservicemetier;

import java.util.List;

import fr.afpa.entitemetier.Annonce;

public interface IServiceAnnonceMetier {

	public List<Annonce> getListAnnonces();

	public Annonce ajoutAnnonce(Annonce annonce, Long idPersonne);

	public Annonce modifierAnnonce(Annonce annonce, Long idAnnonce);

	public Annonce getAnnonce(Long id);

	public List<Annonce> getListAnnonce(Long id);


}
