package fr.afpa.entitemetier;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Note {

	protected Long idNote;
	protected int valeurNote; 
	protected String appreciationNote;
	protected Long idPersonne;
}

