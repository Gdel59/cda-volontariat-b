package fr.afpa.entitemetier;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Authentification {

	private Long idAuthentification;
	private String login;
	private String motDePasse;
	
	
	public Authentification() {
		super();
	}

	public Authentification(Long idAuthentification, String login, String motDePasse) {
		super();
		this.idAuthentification = idAuthentification;
		this.login = login;
		this.motDePasse = motDePasse;
		
	}
	
	public Authentification(String login, String motDePasse) {
		super();
		this.login = login;
		this.motDePasse = motDePasse;
		
	}

	
}
