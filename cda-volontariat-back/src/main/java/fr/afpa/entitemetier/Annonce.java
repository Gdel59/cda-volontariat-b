package fr.afpa.entitemetier;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Annonce {

	protected Long id;
	protected String libelle; 
	protected String descriptif;
	protected LocalDate date;
	protected String type;
	protected Personne personne;
}








	
	
	
	
	




