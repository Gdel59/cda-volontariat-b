package fr.afpa.entitemetier;

public class Volontaire extends Personne {
	
	public Volontaire() {
		super();
		
	}

	public Volontaire(String ville, String nom, String prenom,String email, String tel, String adresse, Authentification authentification, Role role) {
		this.nom=nom;
		this.prenom=prenom;
		this.email=email;
		this.tel=tel;
		this.adresse=adresse;
		this.authentification= authentification;
		this.role=role;
		this.ville=ville;
		
	}
}
