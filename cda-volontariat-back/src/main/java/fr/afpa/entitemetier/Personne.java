package fr.afpa.entitemetier;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = Admin.class, name = "admin"),
    @JsonSubTypes.Type(value = Volontaire.class, name = "volontaire"),
    @JsonSubTypes.Type(value = Client.class, name = "client")
})
public class Personne {
	
	protected Long id;
	protected String nom; 
	protected String prenom;
	protected String tel;
	protected String email;
	protected String adresse;
	protected String ville;
	
	protected Role role;
	protected Authentification authentification;
	
	
	
	

}

