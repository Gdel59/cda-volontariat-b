package fr.afpa.entitemetier;



import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Role {

	private Long idRole;
	private String libelle;
	//private List<Personne> personne;
	
	public Role() {
		super();
	}

	public Role(Long idRole, String libelle) {
		super();
		this.idRole = idRole;
		this.libelle = libelle;
		//this.personne = new ArrayList<Personne>();
	}

	
}
