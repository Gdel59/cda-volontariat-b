package fr.afpa.entitedao;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.afpa.entitemetier.Personne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="annonce")
public class AnnonceDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "annonce_generator")
	@SequenceGenerator(name = "annonce_generator", sequenceName = "annonce_seq",allocationSize = 1,initialValue = 2)
	@Column
	private Long idAnnonce;
	    
	
	@Column
	private String libelle;
	
	@Column(name="texte")
	private String descriptif;

	@Column(name="dateAnnonce")
	private LocalDate date;
	
	@Column(name="typeAnnonce")
	private String type;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="idPersonne")
	private PersonneDao personne;

	
}
