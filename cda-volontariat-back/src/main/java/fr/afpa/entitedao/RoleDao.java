package fr.afpa.entitedao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString


@Entity
@Table(name = "roleprofil")
public class RoleDao {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_generator")
	@SequenceGenerator(name = "role_generator", sequenceName = "role_seq", allocationSize = 1, initialValue = 2)
	@Column(name = "idRole", updatable = false, nullable = false)

	private Long idRole;

	@Column
	private String libelle;

	@JsonIgnore
	@OneToMany(mappedBy = "role")
	//@OnDelete(action = OnDeleteAction.CASCADE)
	private List<PersonneDao> personne;
	
	public RoleDao() {
		super();
	}
	
	public RoleDao(String libelle) {
		this.libelle = libelle;
		personne = new ArrayList<PersonneDao>();
	}

}
