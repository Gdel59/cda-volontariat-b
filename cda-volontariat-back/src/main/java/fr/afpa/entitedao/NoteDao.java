package fr.afpa.entitedao;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name="note")
public class NoteDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "note_generator")
	@SequenceGenerator(name = "note_generator", sequenceName = "note_seq",allocationSize = 1,initialValue = 2)
	@Column
	private Long idNote;
	    
	
	@Column
	private int valeurNote;
	
	@Column
	private String appreciationNote;
	
	@Column
	private Long idPersonne;
}
