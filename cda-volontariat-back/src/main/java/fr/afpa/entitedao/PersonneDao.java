package fr.afpa.entitedao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;




@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

	
	@Entity
	@Table(name = "personne")
	public class PersonneDao {

		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "personne_generator")
		@SequenceGenerator(name = "personne_generator", sequenceName = "seq_personne",allocationSize = 1,initialValue = 2)  
		@Column (name="idPersonne", updatable = false, nullable = false )
		private Long idPersonne;
		
		@Column (name="nom", updatable = true, nullable = false,length = 25)
		private String nom;
		@Column (name="prenom", updatable = true, nullable = false,length = 25)
		private String prenom;
		@Column (name="email", updatable = true, nullable = false,length = 50)
		private String mail;
		@Column (name="telephone", updatable = true, nullable = false,length = 10)
		private String tel;
		@Column (name="adresse", updatable = true, nullable = false,length = 100)
		private String adresse;
		@Column (name="ville", updatable = true, nullable = false, length = 25)
		private String ville;
		
		@OneToMany(mappedBy = "personne", cascade=CascadeType.PERSIST, orphanRemoval =true)
		@OnDelete( action = OnDeleteAction.CASCADE)
		private List<AnnonceDao> annonce;
		
		@ManyToOne(cascade = {CascadeType.MERGE})
		@JoinColumn(name="idRole")
		private RoleDao role;
		
		@OneToOne(mappedBy="personne", cascade=CascadeType.PERSIST, orphanRemoval = true)
		@OnDelete( action = OnDeleteAction.CASCADE )
		private AuthentificationDao authentification;
		
		

}
